module Chapter2.E5 where

import Prelude hiding (init)

lfinit :: [a] -> [a]
lfinit = reverse . tail . reverse

init' :: [a] -> [a]
init' ns = take n ns
  where
    n = length ns - 1

init :: [a] -> [a]
init [n] = []
init (n:ns) = n : init ns
