module Chapter2.E3 where

n :: Int
n = a `div` length xs
  where
    a = 10
    xs = [1..5]
