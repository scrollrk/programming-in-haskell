module Chapter2.E2 where

first :: Integer
first = (2 ^ 3) * 4
second :: Integer
second = (2 * 3) + (4 * 5)
third :: Integer
third = 2 + (3 * (4 ^ 5))
