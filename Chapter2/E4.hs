module Chapter2.E4 where

import Prelude hiding (last, length)
last :: [a] -> a
last [n] = n
last (_:ns) = last ns

lflast :: [a] -> a
lflast = head . reverse

lflast' :: [a] -> a
lflast' xs = xs !! (length xs - 1)

length :: [a] -> Int
length [] = 0
length (x:xs) = 1 + length xs
