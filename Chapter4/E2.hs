module Chapter4.E2 where

halve :: [a] -> ([a],[a])
halve xs = splitAt (length xs `div` 2) xs

athird :: [a] -> a
athird (x:xs) = head . tail $ xs

bthird :: [a] -> a
bthird = (!! 2)

cthird :: [a] -> a
cthird (_:_:x:xs) = x
