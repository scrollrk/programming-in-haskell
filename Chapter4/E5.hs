module Chapter4.E5 where

import Prelude hiding ((&&))

(&&) :: Bool -> Bool -> Bool
a && b = if a then
           if b then True else False
         else False
