module Chapter4.E3 where

asafetail :: [a] -> [a]
asafetail xs = if null xs then [] else tail xs

bsafetail :: [a] -> [a]
bsafetail xs | null xs = []
             | otherwise = tail xs

csafetail :: [a] -> [a]
csafetail [] = []
csafetail xs = tail xs
