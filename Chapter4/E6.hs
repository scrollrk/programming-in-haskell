module Chapter4.E6 where

import Prelude hiding ((&&))

(&&) :: Bool -> Bool -> Bool
a && b = if a then b else False
