module Chapter4.E4 where

(|%) :: Bool -> Bool -> Bool
True  |% True  = True
False |% True  = True
True  |% False = True
False |% False = False

(|^) :: Bool -> Bool -> Bool
True |^ _     = True
_    |^ True  = True
_    |^ _     = False

(|*) :: Bool -> Bool -> Bool
False |* b = b
True  |* _ = True

(|&) :: Bool -> Bool -> Bool
a |& b | a == b = b
       | otherwise = True
