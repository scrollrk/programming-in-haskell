module Chapter4.E7 where

mult :: Num a => a -> (a -> (a -> a))
mult = \x -> (\y ->(\z -> x*y*z))
