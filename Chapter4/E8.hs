module Chapter4.E8 where

luhnDouble :: (Ord a, Num a) => a -> a
luhnDouble n | double > 9 = double - 9
             | otherwise = double
  where double = n * 2

luhn :: Integral a => a -> a -> a -> a -> Bool
luhn n m o p = (luhnDouble n + m + luhnDouble o + p) `mod` 10 == 0
