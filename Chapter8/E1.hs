module Chapter8.E1 where

data Nat = Zero | Succ Nat

nat2int :: Nat -> Int
nat2int Zero = 0
nat2int (Succ n) = 1+nat2int n

int2nat :: Int -> Nat
int2nat 0 = Zero
int2nat n = Succ(int2nat(n-1))

add :: Nat -> Nat -> Nat
add Zero n = n
add (Succ n) m = Succ (add m n)

mult :: Nat -> Nat -> Nat
mult Zero n = Zero
mult (Succ n) m = add m (mult n m)
