module Chapter8.E7 where

import Prelude hiding ( Maybe (..) )

data Maybe a = Nothing | Just a
data List a = Empty | Cons a (List a)

instance Eq a => Eq (Maybe a) where
  Just x == Just y = x == y
  _ == _ = False

instance Eq a => Eq (List a) where
  Cons x xs == Cons y ys = x == y && xs == ys
  Empty == Empty = True
  _ == _ = False
