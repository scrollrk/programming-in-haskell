module Chapter8.E4 where

import Chapter8.E3 ( Tree(..) )

halve :: [a] -> ([a],[a])
halve [] = ([],[])
halve xs = splitAt (length xs `div` 2) xs

balance :: [a] -> Tree a
balance [x] = Leaf x
balance xs = Node (balance ys) (balance zs)
  where (ys,zs) = halve xs
