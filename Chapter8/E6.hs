module Chapter8.E6 where

import Chapter8.E5 ( Expr, folde )

eval :: Expr -> Int
eval = folde id (+)

size :: Expr -> Int
size = folde (const 1) (+)
