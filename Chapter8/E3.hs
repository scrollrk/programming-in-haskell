module Chapter8.E3 where

data Tree a = Leaf a | Node (Tree a) (Tree a) deriving Show

leaves :: Tree a -> Int
leaves (Leaf _) = 1
leaves (Node x y) = leaves x + leaves y

balanced :: Tree a -> Bool
balanced (Leaf x) = True
balanced (Node x y) = abs (leaves x - leaves y) <= 1 && balanced x && balanced y
