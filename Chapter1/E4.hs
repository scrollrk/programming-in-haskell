module Chapter1.E4 where

revqsort :: Ord a => [a] -> [a]
revqsort []     = []
revqsort (x:xs) = revqsort larger ++ [x] ++ revqsort smaller
  where
    smaller = [a | a <- xs, a < x]
    larger  = [b | b <- xs, b >= x]
