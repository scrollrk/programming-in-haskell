sum [x] = x
    {applying sum}
x + sum [] = x
    {applying sum}
x + 0 = x
    {applying +}
x = x
