module Chapter1.E5 where

-- Removing the = from the predicate of smaller and larger removes all duplicate Ords.

nodupqsort :: Ord a => [a] -> [a]
nodupqsort []     = []
nodupqsort (x:xs) = nodupqsort smaller ++ [x] ++ nodupqsort larger
  where
    smaller = [a | a <- xs, a < x]
    larger  = [b | b <- xs, b > x]
