module Chapter1.E3 where

import Prelude hiding (product)

product :: Num a => [a] -> a
product [] = 1
product (n:ns) = n * product ns
