module Chapter11.E2 where

import Chapter11.E1 hiding ( play' )
import System.Random ( randomRIO )

bestmove' :: Grid -> Player -> IO Grid
bestmove' g p = do
  random <- randomRIO (0, length moves-1)
  return (moves !! random)
    where
      tree = prune depth (gametree g p)
      Node (_,best) ts = minimax tree
      moves = [g' | Node (g',p') _ <- ts, p' == best]

play' :: Grid -> Player -> IO ()
play' g p
   | wins O g = putStrLn "Player O wins!\n"
   | wins X g = putStrLn "Player X wins!\n"
   | full g   = putStrLn "It's a draw!\n"
   | p == O   = do i <- getNat (prompt p)
                   case move g i p of
                      []   -> do putStrLn "ERROR: Invalid move"
                                 play' g p
                      [g'] -> play g' (next p)
   | p == X   = do putStr "Player X is thinking... "
                   move <- bestmove' g p
                   (play $! move) (next p)
