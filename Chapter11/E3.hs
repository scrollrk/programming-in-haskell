module Chapter11.E3 where

import Chapter11.E1 hiding ( play' )
import Data.List ( sortOn )

fastestmove :: Grid -> Player -> Grid
fastestmove g p = head [g' | Node (g',p') _ <- sortOn treedepth ts, p' == best]
  where
    tree = prune depth (gametree g p)
    Node (_,best) ts = minimax tree

play' :: Grid -> Player -> IO ()
play' g p
   | wins O g = putStrLn "Player O wins!\n"
   | wins X g = putStrLn "Player X wins!\n"
   | full g   = putStrLn "It's a draw!\n"
   | p == O   = do i <- getNat (prompt p)
                   case move g i p of
                      []   -> do putStrLn "ERROR: Invalid move"
                                 play' g p
                      [g'] -> play g' (next p)
   | p == X   = do putStr "Player X is thinking... "
                   (play $! fastestmove g p) (next p)
