module Chapter11.E4 where

import Data.List ( transpose )
import Chapter11.E1
    ( Tree(..), Player(..), Grid, next, empty, full, diag, putGrid,
      move, cls, goto, getNat, prompt, moves )
size :: Int
size = 3

winLineSize :: Int
winLineSize = 3

gametree :: Grid -> Player -> Tree (Grid,Player)
gametree g p = minimax p (movetree g p)
  where
    movetree :: Grid -> Player -> Tree Grid
    movetree g p = Node g [movetree g' (next p) | g' <- moves g p]

turn :: Player -> Grid -> Player
turn p g | os > xs = X
         | xs > os = O
         | otherwise = p
            where
             os = length (filter (== O) ps)
             xs = length (filter (== X) ps)
             ps = concat g

minimax :: Player -> Tree Grid -> Tree (Grid,Player)
minimax _ (Node g [])
   | wins O g  = Node (g,O) []
   | wins X g  = Node (g,X) []
   | otherwise = Node (g,B) []
minimax p (Node g ts)
   | turn p g == O = Node (g, minimum ps) ts'
   | turn p g == X = Node (g, maximum ps) ts'
                   where
                      ts' = map (minimax p) ts
                      ps  = [p | Node (_,p) _ <- ts']

playerSelect :: IO ()
playerSelect = do
  putStr "Enter desired Player (X/O/x/o)"
  p <- getChar
  case p of
    'O' -> firstturn O
    'o' -> firstturn O
    'X' -> firstturn X
    'x' -> firstturn X
    _ -> do putStrLn "ERROR: Not a valid Player"
            playerSelect

wins :: Player -> [[Player]] -> Bool
wins p g = any line (rows ++ cols ++ dias)
  where
    line = winLineCh p
    rows = g
    cols = transpose g
    dias = [diag g, diag (map reverse g)]

linesize :: Player -> [Player] -> Int
linesize _ [] = 0
linesize p (l:ls) | p == l = 1 + linesize p ls
                  | otherwise = linesize p ls

winLineCh :: Player -> [Player] -> Bool
winLineCh p ls = linesize p ls >= winLineSize

firstturn :: Player -> IO ()
firstturn p = play (gametree empty p) p

play :: Tree (Grid, Player) -> Player -> IO ()
play t p = do cls
              goto (1,1)
              putGrid g
              play' t p
  where g = currentgrid t

play' :: Tree (Grid,Player) -> Player -> IO ()
play' t p
  | wins O g = putStrLn "Player O wins!\n"
  | wins X g = putStrLn "Player X wins!\n"
  | full g   = putStrLn "It's a draw!\n"
  | otherwise = do i <- getNat (prompt p)
                   case move g i p of
                     []   -> do putStrLn "ERROR: Invalid move"
                                play' t p
                     [g'] -> do putStrLn ("Player " ++ show (next p) ++ " is thinking")
                                (play $! bestmove (findgrid g' t)) p
  where g = currentgrid t

currentgrid :: Tree (Grid,Player) -> Grid
currentgrid (Node t ts) = fst t

findgrid :: Grid -> Tree (Grid,Player) -> Tree (Grid,Player)
findgrid g (Node m ms) = head [Node (g',p) ms' | Node (g',p) ms' <- ms, g' == g]

bestmove :: Tree (Grid,Player) -> Tree (Grid,Player)
bestmove t = head [Node (g',p') ms' | Node (g',p') ms' <- ts, p' == c]
  where  Node (_,c) ts = t

tictactoe :: IO ()
tictactoe = playerSelect
