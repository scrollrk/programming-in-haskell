module Chapter12.E3 where

newtype Apply a b = A (a -> b)

instance Functor (Apply a) where
  --fmap :: (b -> c) -> (a -> b) -> (a -> c)
  fmap h (A g) = A (h . g)

instance Applicative (Apply a) where
  --pure :: b -> (a -> b)
  pure x = A (const x)
  --(<*>) :: (a -> (b -> c)) -> (a -> b) -> a -> c
  (A h) <*> (A g) = A(\x -> h x (g x))
