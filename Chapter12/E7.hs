module Chapter12.E7 where

data Expr a = Var a | Val Int | Add (Expr a) (Expr a) deriving Show

instance Functor Expr where
  -- fmap :: (a -> b) -> f a -> f b
  fmap g (Var x) = Var (g x)
  fmap _ (Val n) = Val n
  fmap g (Add l r) = Add (fmap g l) (fmap g r)

instance Applicative Expr where
  -- pure :: a -> f a
  pure = Var
  -- <*> :: f ( a -> b ) -> f a -> f b
  Var g <*> x = g <$> x

instance Monad Expr where
  -- return = pure
  return = pure
  -- >>= :: f a -> (a -> f b) -> f b
  Var x >>= g = g x
  Val x >>= g = Val x
  Add l r >>= g = Add (l >>= g) (r >>= g)
