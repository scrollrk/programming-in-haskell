module Chapter12.E2 where

newtype Apply a b = A (a -> b)

instance Functor (Apply a) where
  --fmap :: (b -> c) -> (a -> b) -> (a -> c)
  fmap h (A g) = A (h . g)
