module Chapter12.E6 where

newtype Apply a b = A (a -> b)

instance Functor (Apply a) where
  --fmap :: (b -> c) -> (a -> b) -> (a -> c)
  fmap h (A g) = A (h . g)

instance Applicative (Apply a) where
  --pure :: b -> (a -> b)
  pure x = A (const x)
  --(<*>) :: (a -> (b -> c)) -> (a -> b) -> a -> c
  (A g) <*> (A h) = A(\x -> g x (h x))

instance Monad (Apply a) where
  -- return = pure
  return = pure
  -- >>= f (a -> b) -> (a -> f (b -> c)) -> f (a -> c)
  -- mx >>= f = f x
  (A f) >>= g = A(\x -> let A h = g (f x) in h x)
