-- |

module Chapter13.E6 where

import Chapter13.E5 ( Parser( P ) , symbol, parse, natural )
import Control.Applicative ( Alternative((<|>)) )

integer :: Parser Int
integer = token int

expr :: Parser Int
expr = do t <- term
          do symbol "+"
             e <- expr
             return (t + e)
           <|> do symbol "-"
                  e <- expr
                  return (t - e)
                <|> return t

term :: Parser Int
term = do f <- factor
          do symbol "*"
             t <- term
             return (f * t)
           <|> do symbol "/"
                  e <- term
                  return (f `div` e)
                <|> return f


factor :: Parser Int
factor = do symbol "("
            e <- expr
            symbol ")"
            return e
          <|> do natural

eval :: String -> Int
eval xs = case parse expr xs of
  [(n,[])] -> n
  [(_,out)] -> error ("Unused input " ++ out)
  [] -> error "Invalid input"
