-- |

module Chapter13.E5 where

import Control.Applicative ( Alternative((<|>), empty, many, some ) )
import Data.Char ( isSpace, isDigit )

type Expr = Int

newtype Parser a = P (String -> [(a,String)])

instance Functor Parser where
  -- fmap :: (a -> b) -> Parser a -> Parser b
  fmap g p = P (\inp -> case parse p inp of
                   [] -> []
                   [(v,out)] -> [(g v, out)])

instance Applicative Parser where
  --pure :: a -> Parser a
  pure v = P (\inp -> [(v,inp)])
  -- <*> :: Parser (a -> b) -> Parser a -> Parser b
  pg <*> px = P (\inp -> case parse pg inp of
                    [] -> []
                    [(g,out)] -> parse (fmap g px) out)
instance Monad Parser where
  -- (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  px >>= f = P (\inp -> case parse px inp of
                   [] -> []
                   [(v,out)] -> parse (f v) out)

instance Alternative Parser where
  -- empty :: Parser a
  empty = P (const [])
  -- (<|>) :: Parser a -> Parser a -> Parser a
  p <|> q = P (\inp -> case parse p inp of
                  [] -> parse q inp
                  [(v,out)] -> [(v,out)])

parse :: Parser a -> String -> [(a,String)]
parse (P p) = p

digit :: Parser Char
digit = sat isDigit

nat :: Parser Int
nat = do xs <- some digit
         return (read xs)

natural :: Parser Int
natural = token nat

token :: Parser a -> Parser a
token p = do space
             v <- p
             space
             return v

item :: Parser Char
item = P (\inp -> case inp of
             [] -> []
             (x:xs) -> [(x,xs)])

sat :: (Char -> Bool) -> Parser Char
sat p = do x <- item
           if p x then return x else empty

space :: Parser ()
space = do many (sat isSpace)
           return ()

char :: Char -> Parser Char
char x = sat (==x)

string :: String -> Parser String
string [] = return []
string (x:xs) = do char x
                   string xs
                   return (x:xs)

symbol :: String -> Parser String
symbol xs = token (string xs)

expr :: Parser Expr
expr = do t <- term
          do symbol "+"
             e <- expr
             return (t + e)
           <|> return t

term :: Parser Expr
term = do f <- factor
          do symbol "*"
             t <- term
             return (f * t)
           <|> return f


factor :: Parser Expr
factor = (symbol "(" >>
         expr >>= \e ->
         symbol ")" >>
         return e) <|> natural

eval :: String -> Int
eval xs = case parse expr xs of
  [(n,[])] -> n
  [(_,out)] -> error ("Unused input " ++ out)
  [] -> error "Invalid input"
