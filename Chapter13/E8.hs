-- |

module Chapter13.E8 where

import Chapter13.E5 ( Parser( P ) ,symbol, parse, natural )
import Control.Applicative ( Alternative((<|>), empty,many) )

--a. expr ::= ( expr + | expr - | e ) term
--b.

badexpr :: Parser Int
badexpr = do t <- term
             do symbol "+"
                e <- badexpr
                return (t + e)
              <|> do symbol "-"
                     e <- badexpr
                     return (t - e)

--c. Parser doesn't know what to do with just a term therfore it never stops looping.
--d. many and foldl

expr :: Parser Int
expr = do t <- term
          es <- many expr'
          return (foldl (+) t es)
  where
    expr' :: Parser Int
    expr' = do symbol "+"
               term
              <|> do symbol "-"
                     e <- term
                     return (-e)

term :: Parser Int
term = do f <- exponintial
          do symbol "*"
             t <- term
             return (f * t)
           <|> do symbol "/"
                  e <- term
                  return (f `div` e)
                <|> return f

exponintial :: Parser Int
exponintial = do f <- factor
                 do symbol "^"
                    e <- exponintial
                    return (f ^ e)
                   <|> return f


factor :: Parser Int
factor = do symbol "("
            e <- expr
            symbol ")"
            return e
          <|> integer

eval :: String -> Int
eval xs = case parse expr xs of
  [(n,[])] -> n
  [(_,out)] -> error ("Unused input " ++ out)
  [] -> error "Invalid input"
