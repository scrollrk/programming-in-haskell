module Chapter9.E6 where

import Chapter9.E1 ( choices )
import Data.List ( sortOn )

data Op = Add | Sub | Mul | Div | Exp

instance Show Op where
  show Add = "+"
  show Sub = "-"
  show Mul = "*"
  show Div = "/"
  show Exp = "^"

valid :: Op -> Int -> Int -> Bool
valid Add x y = x <= y
valid Sub x y = x > y
valid Mul x y = x /= 1 && y /= 1 && x <= y
valid Div x y = y > 0 && y /= 1 && x `mod` y == 0
valid Exp x y = y /= 1 && y > 0

apply :: Op -> Int -> Int -> Int
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y
apply Exp x y = x ^ y

data Expr = Val Int | App Op Expr Expr

instance Show Expr where
  show (Val n) = show n
  show (App o l r) = brak l ++ show o ++ brak r ++ "=" ++ show (head $ eval (App o l r))
    where
      brak (Val n) = show n
      brak e = "(" ++ show e ++ ")"

values :: Expr -> [Int]
values (Val n) = [n]
values (App _ l r) = values l ++ values r

eval :: Expr -> [Int]
eval (Val n) = [n | n > 0]
eval (App o l r) = [apply o x y | x <- eval l, y <- eval r, valid o x y]

split :: [a] -> [([a],[a])]
split [] = []
split [_] = []
split (x:xs) = ([x],xs) : [(x:ls,rs) | (ls,rs) <- split xs]

ops :: [Op]
ops = [Add,Sub,Mul,Div,Exp]

type Result = (Expr, Int)

results :: [Int] -> [Result]
results [] = []
results [n] = [(Val n,n) | n > 0]
results ns = [res | (ls,rs) <- split ns,
                     ls <- results ls,
                     ry <- results rs,
                     res <- combine ls ry]

combine :: Result -> Result -> [Result]
combine (l,x) (r,y) = [(App o l r, apply o x y) | o <- ops, valid o x y]

possibleSolutions :: [Int] -> Int -> [(Int,Expr)]
possibleSolutions ns n = [(m,e) | ns' <- choices ns, (e,m) <- results ns']

closest :: Int -> [(Int,Expr)] -> [Expr]
closest n ps = map snd $ nearestAnswers n $ sortOn fst ps

nearestAnswers :: Int -> [(Int,Expr)] -> [(Int,Expr)]
nearestAnswers _ [] = []
nearestAnswers n [(x,e)]           = [(x,e)]
nearestAnswers n ((x,e):(y,e'):xs) | n < y && n > x = [(x,e),(y,e')]
                                   | n > x = nearestAnswers n ((y,e') : xs)
                                   | otherwise = [(x,e)]

simplicity :: Expr -> Int
simplicity (Val n) = 1
simplicity (App o l r) = opp o + simplicity l + simplicity r
  where
    opp Add = 2
    opp Sub = 2
    opp Mul = 3
    opp Div = 3
    opp Exp = 5

solutions :: [Int] -> Int -> [Expr]
solutions ns n | not $ null cs = sortOn simplicity cs
               | otherwise = closest n ps
  where
    ps = possibleSolutions ns n
    cs = [e | (a,e) <- ps, a == n]
