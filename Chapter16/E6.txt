data Tree = Leaf Int | Node Tree Tree

leafs :: Tree -> Int
leafs (Leaf x) = 1
leafs (Node l r) = leafs l + leafs r

nodes :: Tree -> Int
nodes (Leaf x) = 0
nodes (Node l r) = nodes l + 1 + nodes r

Base case:

nodes (Leaf x) + 1
    {applying nodes}
0 + 1
    {unapplying leafs}
0 + leafs (Leaf x)
    {applying +}
leafs (leaf x)


Inductive case:

nodes (Node l r) + 1
    {applying nodes}
nodes l + 1 + nodes r + 1
    {induction hypotheisis on nodes l + 1}
leafs l + nodes r + 1
    {induction hypotheisis on nodes r + 1}
leafs l + leafs r
    {unapplying leafs}
leafs (Node l r)
