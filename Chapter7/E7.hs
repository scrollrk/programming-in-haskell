module Chapter7.E7 where

import Data.Char ( ord, chr )

type Bit = Int

bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x+y*2) 0

int2bin :: Int -> [Bit]
int2bin 0 = []
int2bin n = n `mod` 2 : int2bin (n `div` 2)

make8 :: [Bit] -> [Bit]
make8 bs = take 8 (bs ++ repeat 0)

addParity :: [Bit] -> [Bit]
addParity bs = bs ++ [parityBit bs]

checkParity :: [Bit] -> [Bit]
checkParity bs | parityBit t8 == last bs = t8
               | otherwise = errorWithoutStackTrace "Parity check bad"
  where
    t8 = take 8 bs

parityBit :: [Bit] -> Bit
parityBit bs | parity bs = 1
             | otherwise = 0

parity :: [Bit] -> Bool
parity bs = odd . length $ filter (==1) bs

encode :: String -> [Bit]
encode = concatMap (addParity . make8 . int2bin . ord)

chop8 :: [Bit] -> [[Bit]]
chop8 [] = []
chop8 bs = take 8 bs : chop8(drop 8 bs)

chop9 :: [Bit] -> [[Bit]]
chop9 [] = []
chop9 bs = checkParity (take 9 bs) : chop9(drop 9 bs)

decode :: [Bit] -> String
decode = map (chr . bin2int) . chop9

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = id
