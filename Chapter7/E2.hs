module Chapter7.E2 where

import Prelude hiding (all, any, takeWhile, dropWhile)

--a.
all :: (a -> Bool) -> [a] -> Bool
all p = and . map p

all' :: (a -> Bool) -> [a] -> Bool
all' p = foldl (\xs x -> p x && xs) True

--b.
any :: (a -> Bool) -> [a] -> Bool
any p = or . map p

any' :: (a -> Bool) -> [a] -> Bool
any' p = foldl (\xs x -> p x || xs) False

--c.
takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile p (x:xs) | p x = x : takeWhile p xs
                   | otherwise = []

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' p = foldr (\x xs -> if p x then x:xs else []) []

--d.
dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile p (x:xs) | p x = dropWhile p xs
                   | otherwise = x:xs

dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' p = foldr(\x xs -> if p x then xs else x:xs) []
