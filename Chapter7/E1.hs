module Chapter7.E1 where

import Prelude hiding (filterMap)

filterMap :: (a -> b) -> (a -> Bool) -> [a] -> [b]
filterMap f p = map f . filter p
