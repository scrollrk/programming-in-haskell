module Chapter7.E4 where

dec2int :: [Int] -> Int
dec2int = foldl (\a x -> x+a*10) 0
