module Chapter7.E8 where

import Chapter7.E7 ( encode, decode, channel )

transmit' :: String -> String
transmit' = decode . channel . tail . encode
