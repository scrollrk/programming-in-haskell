module Chapter7.E10 where

import Chapter7.E9 ( altMap )

luhnDouble :: (Ord a, Num a) => a -> a
luhnDouble n | double > 9 = double - 9
             | otherwise = double
  where double = n * 2

luhn :: [Int] -> Bool
luhn xs = (sum . altMap id luhnDouble $ reverse xs) `mod` 10 ==0
