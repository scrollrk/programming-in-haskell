module Chapter15.E4 where

fibs :: [Integer]
fibs = 0 : 1 : [x+y | (x,y) <- zip ns (tail ns)]
  where
    ns = fibs
