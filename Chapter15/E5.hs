module Chapter15.E5 where

import           Prelude hiding (repeat, replicate, take)

data Tree a = Leaf | Node (Tree a) a (Tree a)
  deriving Show

repeat :: a -> Tree a
repeat x = Node (repeat x) x (repeat x)

take :: Int -> Tree a -> Tree a
take 0 _ = Leaf
take _ Leaf = Leaf
take n (Node l x r) = Node (take n' l) x (take n' r)
  where n' = n - 1

replicate :: Int -> a -> Tree a
replicate n = take n . repeat
