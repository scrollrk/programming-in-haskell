module Chapter15.E6 where

next :: Double -> Double -> Double
next n a = (a + n/a) / 2

sqroot :: Double -> Double
sqroot n = snd . last $ takeWhile p (zip l $ tail l)
  where
    l = iterate (next n) 1.0
    p (n',m) = abs (n' - m) > 0.00001
