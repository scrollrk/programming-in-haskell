module Chapter6.E3 where

import Prelude hiding ((^))

(^) :: Int -> Int -> Int
a ^ b | b < 0 = errorWithoutStackTrace "Negative Exponent"
      | b == 0 = 1
      | b == 1 = a
      | otherwise = a * (a ^ (b-1))
