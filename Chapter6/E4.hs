module Chapter6.E4 where

euclid :: Int -> Int -> Int
euclid a b | b > a = euclid a (b - a)
           | a > b = euclid (a - b) b
           | otherwise = a

reduce :: Int -> Int -> [Char]
reduce a b = show (a `div` gcd) ++ "/" ++ show (b `div` gcd)
  where gcd = euclid a b
