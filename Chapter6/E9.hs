module Chapter6.E9 where

import Prelude hiding (sum, take, last)

sum :: Num a => [a] -> a
sum [] = 0
sum (x:xs) = x + sum xs

take :: Integral a => a -> [b] -> [b]
take 0 _ = []
take _ [] = []
take n (x:xs) = x : take (n-1) xs

last :: [a] -> a
last [] = error "Last of empty list"
last [x] = x
last (_:xs) = last xs
