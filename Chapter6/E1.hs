module Chapter6.E1 where

factorial :: (Integral a) => a -> a
factorial n | n < 1 = 1
            | otherwise = n * factorial (n-1)
