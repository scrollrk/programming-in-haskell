module Chapter6.E6 where

import Prelude hiding (and, concat, replicate, (!!), elem)

and :: [Bool] -> Bool
and [] = False
and [x] = x
and (x:xs) | not x = False
           | otherwise = and xs

concat :: [[a]] -> [a]
concat [] = []
concat ([]:xss) = concat xss
concat ((x:xs):xss) = x : concat(xs:xss)

replicate :: Int -> a -> [a]
replicate 0 _ = []
replicate n x = x : replicate (n-1) x

(!!) :: [a] -> Int -> a
[]     !! _ = errorWithoutStackTrace "Index of empty list"
(x:xs) !! 0 = x
(x:xs) !! n = xs !! (n-1)

elem :: Eq a => a -> [a] -> Bool
elem _ [] = False
elem e (x:xs) | e == x = True
              | otherwise = elem e xs
