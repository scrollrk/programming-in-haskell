module Chapter6.E2 where

sumdown :: Int -> Int
sumdown n | n < 1 = 0
          | otherwise = n + sumdown (n-1)
