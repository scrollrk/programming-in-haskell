module Chapter6.E8 where

import Chapter6.E7 ( merge )

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge (msort (fst half)) (msort (snd half))
  where
    half = halve xs

halve :: [a] -> ([a],[a])
halve [] = ([],[])
halve xs = (take h xs, drop h xs)
  where
    h = length xs `div` 2
