module Chapter5.E4 where

replicate' :: Int -> a -> [a]
replicate' n x = [x | _ <- [1..n]]
