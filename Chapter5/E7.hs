module Chapter5.E7 where

comp :: [(Integer, Integer)]
comp = [(x,y) | x <- [1,2], y <- [3,4]]

nestcomp :: [(Integer, Integer)]
nestcomp = concat [[(x,y) | x <- [1,2]] | y <- [3,4]]
