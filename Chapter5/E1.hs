module Chapter5.E1 where

square100 :: Integer
square100 = sum [x^2 | x <- [1..100]]
