module Chapter5.E6 where

factors :: Integral a => a -> [a]
factors n = [x | x <- [1..n], n `mod` x == 0]

perfects :: Int -> [Int]
perfects n = [x | x <- [1..n], 2*x == sum (factors x)]
