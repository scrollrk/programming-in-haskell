module Chapter5.E10 where

import Data.Char ( ord, chr, isLower, isUpper )
import Chapter5.E8 ( positions )

lowers :: String -> Int
lowers xs = length [x | x <- xs, 'z' >= x, x >= 'a']

letters :: [Char] -> Int
letters xs = length [x | x <- xs, 'z' >= x && x >= 'a' || 'Z' >= x && x >= 'A']

count :: Eq a => a -> [a] -> Int
count x xs = length [x' | x' <- xs, x == x']

let2int :: Char -> Int
let2int c = l - ord 'A'
  where
    n = ord c
    l | isLower c = n - 6
      | otherwise = n

int2let :: Int -> Char
int2let n = chr (ord 'A' + n')
  where
    n' | n > 25 = n + 6
       | otherwise = n

shift :: Int -> Char -> Char
shift n c | isLower c = int2let (l + 26)
          | isUpper c = int2let l
          | otherwise = c
  where l = (let2int c + n) `mod` 26

encode :: Int -> String -> String
--encode n xs = [shift n x | x <- xs]
encode n = map (shift n)

table :: [Float]
table = [8.1, 1.5, 2.8, 4.2, 12.7, 2.2, 2.0, 6.1, 7.0,
         0.2, 0.8, 4.0, 2.4, 6.7, 7.5, 1.9, 0.1, 6.0,
         6.3, 9.0, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1]

percent :: Int -> Int -> Float
percent n m = (fromIntegral n / fromIntegral m) * 100

freqs :: String -> [Float]
freqs xs = zipWith (+) [percent (count x xs) n | x <- ['a'..'z']] [percent (count x xs) n | x <- ['A'..'Z']]
  where n = letters xs

chisqr :: Fractional a => [a] -> [a] -> a
chisqr os es = sum [((o-e)^2)/e |  (o,e) <- zip os es]

rotate :: Int -> [a] -> [a]
rotate n xs = drop n xs ++ take n xs

crack xs = encode (-factor) xs
  where
    factor = head (positions (minimum chitab) chitab)
    chitab = [chisqr (rotate n table') table | n <- [0..25]]
    table' = freqs xs
