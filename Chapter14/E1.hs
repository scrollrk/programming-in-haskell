-- |

module Chapter14.E1 where

newtype Pair a b = Pair (a, b)

getPair (Pair (x, y)) = (x, y)

instance (Semigroup a, Semigroup b) => Semigroup (Pair a b) where
  -- <> :: (a,b) -> (a,b) -> (a,b)
  Pair (x1,y1) <> Pair (x2,y2) = Pair (x1 <> x2, y1 <> y2)

instance (Monoid a, Monoid b) => Monoid (Pair a b) where
  -- mempty :: (a,b)
  mempty = Pair (mempty, mempty)
