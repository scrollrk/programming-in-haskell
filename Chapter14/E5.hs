-- |

module Chapter14.E5 where

import Data.Foldable

filterF :: Foldable t => (a -> Bool) -> t a -> [a]
filterF p = foldMap (\x -> if p x then [x] else mempty)

filter_ :: (a -> Bool) -> [a] -> [a]
--filter_ _ [] = []
--filter_ p (x:xs) | p x = x : filter_ p xs
--                 | otherwise filter_ p xs
filter_ p = foldr (\x v -> if p x then x : v else v) []
