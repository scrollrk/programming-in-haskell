-- |

module Chapter14.E4 where

import Data.Foldable

data Tree a = Leaf | Node (Tree a) a (Tree a) deriving Show

instance Functor Tree where
  -- fmap :: (a -> b) -> Tree a -> Tree b
  fmap g Leaf = Leaf
  fmap g (Node l x r) = Node (fmap g l) (g x) (fmap g r)

instance Applicative Tree where
  -- pure :: a -> Tree a
  pure x = Node Leaf x Leaf
  -- (<*>) :: Tree (a -> b) -> Tree a -> Tree b
  Leaf <*> _ = Leaf
  _ <*> Leaf = Leaf
  (Node _ g _) <*> (Node l x r) = Node (g <$> l) (g x) (g <$> r)

instance Monad Tree where
  -- (>>=) :: ) Tree a -> (a -> Tree b) -> Tree b
  Leaf >>= g = Leaf
  (Node l x r) >>= g = Node (l >>= g) (let Node _ x' _ = g x in x') (r >>= g)

instance Semigroup a => Semigroup (Tree a) where
  -- (<>) :: Monoid a => Tree a -> Tree a -> Tree a
 Leaf <> x = x
 x <> Leaf = x
 (Node l1 x1 r1) <> (Node l2 x2 r2) = Node (l1 <> l2) (x1 <> x2) (r1 <> r2)

instance Monoid a => Monoid (Tree a) where
  -- mempty :: Tree a
  mempty = Leaf

instance Foldable Tree where
  -- fold :: Monoid a => Tree a -> a
 fold Leaf = mempty
 fold (Node l x r) = fold l <> x <> fold r

 -- foldMap :: Monoid m => (a -> m) -> Tree a -> m
 foldMap g Leaf = mempty
 foldMap g (Node l x r) = foldMap g l <> g x <> foldMap g r

 -- foldr :: (a -> b -> b) -> b -> Tree a -> b
 foldr _ v Leaf = v
 foldr g v (Node l x r) = foldr g (g x (foldr g v r)) l

 -- foldl :: (a -> b -> a) -> a -> Tree b -> a
 foldl _ v Leaf = v
 foldl g v (Node l x r) = foldl g (g (foldl g v l) x) r
