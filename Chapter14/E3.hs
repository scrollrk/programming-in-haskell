-- |

module Chapter14.E3 where

import Data.Foldable ( Foldable(fold, foldl, foldr, foldMap) )
import Prelude hiding ( Maybe ( .. ) )

data Maybe a = Nothing | Just a deriving Show

instance Functor Maybe where
  -- fmap :: (a -> b) -> Maybe a -> Maybe b
  fmap f (Just x) = Just (f x)
  fmap _ Nothing = Nothing

instance Applicative Maybe where
  -- pure :: a -> Maybe a
  pure = Just

  -- <*> :: Maybe (a -> b) -> Maybe a -> Maybe  b
  (Just f) <*> (Just x) = Just (f x)
  Nothing <*> _ = Nothing
  _ <*> Nothing = Nothing

instance Semigroup a => Semigroup ( Maybe a ) where
  -- <> :: Maybe a -> Maybe a -> Maybe a
  Nothing <> Just x = Just x
  Just x <> Nothing = Just x
  Just x <> Just y = Just (x <> y)

instance Monoid a => Monoid ( Maybe a ) where
  -- mempty :: Maybe a
 mempty = Nothing

instance Foldable Maybe where
  -- fold :: Maybe a -> a
  fold (Just x) = x
  fold Nothing = mempty

  -- foldMap :: Monoid b => (a -> b) -> Maybe a -> b
  foldMap f (Just x) = f x
  foldMap _ Nothing = mempty

  --foldr :: (a -> b -> b) -> b -> Maybe a -> b
  foldr f v (Just x) = f x v
  foldr _ v Nothing = v

  --foldl :: (a -> b -> a) -> a -> Maybe b -> a
  foldl f v (Just x) = f v x
  foldl _ v Nothing = v

instance Traversable Maybe where
  -- traverse :: Applicative f => (a -> f b) -> Maybe a -> f (Maybe b)
  traverse _ Nothing = pure Nothing
  traverse g (Just x) = Just <$> g x
