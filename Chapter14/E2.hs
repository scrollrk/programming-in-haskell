-- |

module Chapter14.E2 where

newtype A a b = Apply (a -> b)

instance Semigroup b => Semigroup (A a b) where
  -- <> :: (a -> b) -> (a -> b) -> a -> b
  Apply f <> Apply g = Apply (\x -> f x <> g x)

instance Monoid b => Monoid (A a b) where
  -- mempty :: (a,b)
  mempty = Apply (const mempty)
