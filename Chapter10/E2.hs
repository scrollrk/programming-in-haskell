module Chapter10.E2 where

type Board = [Int]

putRow :: Int -> Int -> IO ()
putRow row num = do putStr (show row)
                    putStr ": "
                    putStrLn (concat (replicate num "* "))

putBoard :: Board -> IO ()
putBoard = putRows 1

putRows :: Int -> Board -> IO ()
putRows _ [] = return ()
putRows n (x:xs) = do
  putRow n x
  putRows (n+1) xs
