module Chapter10.E1 where

import Prelude hiding ( putStr )

putStr :: String -> IO ()
putStr xs = sequence_ [putChar x | x <- xs]
