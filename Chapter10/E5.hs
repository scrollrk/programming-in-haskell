module Chapter10.E5 where

import Chapter10.E4 ( getDigit )

adder :: IO ()
adder = do
  putStr "How many numbers? "
  n <- getDigit
  ns <- sequence [getDigit | x <- [1..n]]
  putStrLn $ "The total is " ++ show (sum ns) ++ "\n"
