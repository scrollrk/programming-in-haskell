module Chapter10.E4 where

import Data.Char ( digitToInt, isDigit )

adder :: IO ()
adder = do
  putStr "How many numbers? "
  n <- getDigit
  ns <- getDigits n
  let total = ns
  putStrLn ("The total is " ++ show total)

getDigits :: Int -> IO Int
getDigits 0 = return 0
getDigits n = do
  d <- getDigit
  acc <- getDigits (n-1)
  return (d + acc)

getDigit :: IO Int
getDigit = do
  x <- getChar
  putStr "\n"
  if isDigit x then
    return (digitToInt x)
  else
     do putStrLn "ERROR: Invalid digit"
        getDigit
