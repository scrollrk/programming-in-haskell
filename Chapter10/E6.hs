{-# OPTIONS_GHC -Wno-overlapping-patterns #-}
module Chapter10.E6 where

import System.IO (hSetEcho, stdin)

getCh :: IO Char
getCh = do
  hSetEcho stdin  False
  x <- getChar
  hSetEcho stdin True
  return x

readLine :: IO String
readLine = readChar []

readChar :: String -> IO String
readChar s = do c <- getCh
                case c of '\n' -> return s
                          '\DEL' -> if null s then
                                     readChar []
                                   else
                                     do putStr "\b \b"
                                        readChar (init s)
                          _ -> do putChar c
                                  readChar (s ++ [c])
